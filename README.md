
environment=SERVER

# Sparks

Foobar is a Python library for dealing with word pluralization.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install foobar.

```bash
pip install Sparks
```

## Usage

```python
import foobar

# returns 'local'
foobar.pluralize('local')

# returns 'geese'
foobar.pluralize('goose')

# returns 'local'
foobar.singularize('local')

# returns 'server'


## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
